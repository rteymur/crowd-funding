<?php
    ini_set("display_errors", 1);

    class DB {
        public $host;
        public $user;
        public $password;
        public $db_name;

        function __construct($host, $user, $password, $db_name) {
            $this->host = $host;
            $this->user = $user;
            $this->password = $password;
            $this->db_name = $db_name;
        }

        function queryExec($query){
            $link = mysqli_connect($this->host, $this->user, $this->password, $this->db_name);
            if (!$link) {
                die("Connection failed: " . mysqli_connect_error());
            }
            mysqli_set_charset($link, "utf8");
            $result = mysqli_query($link, $query);
            return $result;
        }
    }

    $link = new DB("localhost:3306", "root", "", "hw_project");

    // $host = "localhost:3306";
    // $user = "root";
    // $password = "";
    // $db = "hw_project";

    // $link = mysqli_connect($host, $user, $password, $db);

    // if (!$link) {
    //     die("Connection failed: " . mysqli_connect_error());
    // }

    // mysqli_set_charset($link, "utf8");



?>
<?php
    include('connectDB.php');
    
    function isOwner($user_id, $project_id, $link_){
        // $res = mysqli_query($link_, "SELECT idUser FROM projects WHERE idUser=\"$user_id\" AND idProject=\"$project_id\";");
        $res = $link_ -> queryExec("SELECT idUser FROM projects WHERE idUser=\"$user_id\" AND idProject=\"$project_id\";");
        if (mysqli_num_rows($res) == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    function projectsBudget($project_id, $link_){
        // $totalMoney = mysqli_fetch_assoc(mysqli_query($link_, "SELECT SUM(investmentFund) totalSum
        // FROM projects_investors
        // WHERE idProject='$project_id';"))['totalSum'];
        $totalMoney = mysqli_fetch_assoc($link_ -> queryExec("SELECT SUM(investmentFund) totalSum
        FROM projects_investors
        WHERE idProject='$project_id';"))['totalSum'];
        return $totalMoney;
    }

    function getProjectDetails($pr_id, $link_){
        // $projectDetailsArray = mysqli_fetch_assoc(mysqli_query($link_, "SELECT * FROM projects
        // WHERE idProject='$pr_id';"));
        $projectDetailsArray = mysqli_fetch_assoc($link_ -> queryExec("SELECT * FROM projects
        WHERE idProject='$pr_id';"));
        return $projectDetailsArray;
    }

    function hasDonated($user_id, $project_id, $link_){
        // $userDonateCheck = mysqli_query($link_, "SELECT * FROM projects_investors
        // WHERE idUser='$user_id' AND idProject='$project_id';");
        $userDonateCheck = $link_ -> queryExec("SELECT * FROM projects_investors
        WHERE idUser='$user_id' AND idProject='$project_id';");
        if(mysqli_num_rows($userDonateCheck)!=0){
            return True;
        } else {
            return False;
        }
    }

    function expiredProject($project_id, $link_){
        $today = date("Y-m-d");
        // $query = mysqli_fetch_assoc(mysqli_query($link_, "SELECT projectEndDate FROM projects WHERE idProject='$project_id';"));
        $query = mysqli_fetch_assoc($link_ -> queryExec("SELECT projectEndDate FROM projects WHERE idProject='$project_id';"));
        $endDate = $query['projectEndDate'];
        if($endDate<$today){
            return True;
        }else{
            return False;
        }
    }

?>
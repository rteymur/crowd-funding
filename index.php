<?php
    include('checkAuth.php');
    // include('connectDB.php');
    include('commonFunctions.php');

    $user_id = $_SESSION['user_id'];
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
    $email = $_SESSION['email'];

    // $allpr = mysqli_query($link, "SELECT * FROM projects
    // JOIN users
    // USING (idUser);");
    $query = "SELECT * FROM projects
    JOIN users
    USING (idUser);";

    $allpr = $link -> queryExec($query);

    if(isset($_POST['button1'])) {
        header("Location: login.php");
        session_destroy();
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" 
    crossorigin="anonymous">
    <title>Home</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Crowd-funding</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <?php
            echo "<a href=\"profile.php?id=".($user_id)."\" class=\"nav-link\">Profile</a>";
            ?>
            <form method="post">
                <input type="submit" name="button1" value="Log out" class="btn btn-primary position-absolute top-0 end-0 mx-2 my-2">
            </form> 
        </div>
        
        </div>
    </div>
    </nav>
    <div class="mx-4 my-4">
    <div class="row">
    <?php
        // owner can be name of user instead of iduser
        while($row = mysqli_fetch_assoc($allpr)){
            echo "<div class=\"col-md-4 col-sm-12 mb-4\">";
            echo "<div class=\"card\" >";
            echo "<div class=\"card-body\">";
            if(expiredProject($row['idProject'], $link)==1){
            echo "<h5 class=\"card-title\">".($row['projectName'])." - <span class=\"fw-bold\">Expired</span></h5>";}
            else{
            echo "<h5 class=\"card-title\">".($row['projectName'])."</h5>";
            }
            echo "<p class=\"card-text\">Start date: ".($row['projectStartDate'])."</p>";
            echo "<p class=\"card-text\">End date: ".($row['projectEndDate'])."</p>";
            echo "<p class=\"card-text\">Requested fund: ".($row['requestedFund'])."$</p>";
            echo "<p class=\"card-text\">Owner: ".($row['firstname']." ".$row['lastname'])."</p>";
            
            if(isOwner($user_id, $row['idProject'], $link)==1){
                echo "<a href=\"projectDetailUser.php?idProject=".($row['idProject'])."\" class=\"btn btn-info\">See Details</a></div></div></div>";
            }
            else if(hasDonated($user_id, $row['idProject'], $link)==1){
                echo "<a href=\"projectDetailUser.php?idProject=".($row['idProject'])."\" class=\"btn btn-outline-warning\">Get info</a></div></div></div>";
            }
            else if(expiredProject($row['idProject'], $link)==1){
                echo "<a href=\"projectDetailUser.php?idProject=".($row['idProject'])."\" class=\"btn btn-outline-danger\">Get info</a></div></div></div>";
            }
            else{
                echo "<a href=\"projectDetailUser.php?idProject=".($row['idProject'])."\" class=\"btn btn-success\">Invest</a></div></div></div>";
            }
        }
    ?>
    
    </div>
    </div>
    <img src="maingraph.php" width="1000" height="500" class="img-fluid rounded mx-auto d-block">

    <!-- Footer -->
    <footer class="page-footer font-small blue">

    <div class="footer-copyright text-center py-3">© 2021: Created by Teymur Rzali CS-019, Elvin Rzayev CE-019
    </div>

    </footer>
    
</body>
</html>

<?php


?>